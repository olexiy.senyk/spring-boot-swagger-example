# springfox-swagger2:2.9.2

## Initial state

### Dependencies
  - io.springfox:springfox-swagger2:2.9.2
  - io.springfox:springfox-swagger-ui:2.9.2

### Available URLs
  - [api-docs](http://localhost:8080/v2/api-docs)
  - [Swagger UI](http://localhost:8080/swagger-ui.html)
