package ua.soo.spring.boot.swagger;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ExampleEntity {

    @NotBlank
    private String id;

    @NotBlank
    private String name;

    private String value;
}
