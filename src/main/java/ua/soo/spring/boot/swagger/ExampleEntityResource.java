package ua.soo.spring.boot.swagger;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Null;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@RequestMapping("/api")
public class ExampleEntityResource {

    private Map<String, ExampleEntity> store = new ConcurrentHashMap<>();

    @GetMapping
    public Collection<ExampleEntity> findAll() {
        return store.values();
    }

    @GetMapping("/{id}")
    public ExampleEntity findById(@PathVariable String id) {
        return store.get(id);
    }

    @PostMapping()
    public ResponseEntity<Null> create(@RequestBody @Validated ExampleEntity entity) {
        store.put(entity.getId(), entity);
        return ResponseEntity.ok(null);
    }
}
